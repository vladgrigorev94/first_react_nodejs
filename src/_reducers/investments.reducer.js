import { userConstants } from '../_constants';

export function investments(state = [], action) {
    switch (action.type) {
        case userConstants.GETINVESTMENTS_REQUEST:
            return []
        case userConstants.GETINVESTMENTS_SUCCESS:
            return action.investments;
        default:
            console.log('action ', action );
            return state
    }
}