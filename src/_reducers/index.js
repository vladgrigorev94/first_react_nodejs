import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { investments } from './investments.reducer';
import { transactions } from './transactions.reducer';

const rootReducer = combineReducers({
  authentication,
  registration,
  users,
  alert,
  investments,
  transactions
});

export default rootReducer;