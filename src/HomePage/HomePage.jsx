import React from 'react';
import {connect} from 'react-redux';
import 'antd/dist/antd.css';
import {userActions} from '../_actions';
import {Row, Col, Divider} from 'antd';

class HomePage extends React.Component {
    async componentDidMount() {
        await this.props.getTransactions();
        await this.props.getInvestments();
        this.forceUpdate();
    }

    render() {
        const listItemsLeft = this.props.investments.map(function (item, idx) {
            return (<div key={idx}>
                <Row>
                    <h3>Statement</h3>
                </Row>
                <Row style={{
                    marginTop: idx === 0 ? '45px' : '23px',
                }}>
                    <Col span={12}>
                        <b>Cycle</b>
                    </Col>
                    <Col span={12}>
                        {formatDate(item.created_dt)}
                    </Col>

                </Row>
                <Row>
                    <Col span={12}>
                        <b>Stake Tx</b>
                    </Col>
                    <Col span={12}>
                        {item.tx_amt}
                    </Col>
                </Row>
                <Row style={{
                    marginTop: '40px',
                }}>
                    <Col span={12}>
                        <b>Net stakes</b>
                    </Col>
                    <Col span={12}>
                        {item.net_amt}
                    </Col>
                </Row>
                <Row style={{
                    marginTop: '23px',
                }}>
                    <Col span={12}>
                        <b>Return Tx</b>
                    </Col>
                    <Col span={12}>
                        {item.tx_returned_amt}
                    </Col>
                </Row>
                <Row style={{
                    marginBottom: '40px',
                }}>
                    <Col span={12}>
                        <b>Reward</b>
                    </Col>
                    <Col span={12}>
                        {item.tx_returned_amt - item.tx_amt}
                    </Col>
                </Row>
            </div>)
        })

        const listItemsRight = this.props.transactions.map(function (item, idx) {
            return (
                <Row style={{
                    flexWrap: 'unset',
                    marginTop: '20px',
                }}>
                    <Col span={8}>
                        <Row>
                            <Col span={24}>
                                <b>name</b>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                <b>wallet</b>
                            </Col>
                        </Row>
                        <Row style={{
                            marginTop: '20px',
                        }}>
                            <b>Incoming Reward</b>
                        </Row>
                        <Row>
                            <Col span={24}>
                                <b>Incoming Tx</b>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                <b>Balance</b>
                            </Col>
                        </Row>
                        <Row style={{
                            marginTop: '20px',
                        }}>
                            <Col span={24}>
                                Cycle in (staked)
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                Stake next cycle
                            </Col>
                        </Row>
                        <Row style={{
                            marginTop: '20px',
                        }}>
                            <Col span={24}>
                                <b>Reward</b>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                Cycle out
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                <b>Total</b>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={8} key={idx}>
                        <Row>
                            <b>Acc{idx}</b>
                        </Row>
                        <Row>
                            {item.user_id ? item.user_id : 0}
                        </Row>
                        <Row style={{
                            marginTop: '20px',
                        }}>
                            <Col span={12}>
                                {item.incoming_reward ? item.incoming_reward : 'no data'}
                            </Col>
                        </Row>
                        <Row>
                            <Col span={12}>
                                {item.incoming_tx ? item.incoming_tx : '0'}
                            </Col>
                        </Row>
                        <Row>
                            <Col span={12}>
                                {item.balance_amt ? item.balance_amt : '0'}
                            </Col>
                        </Row>
                        <Row style={{
                            marginTop: '20px',
                        }}>
                            <Col span={12}>
                                {item.cycle_in ? item.cycle_in : '0'}
                            </Col>
                        </Row>
                        <Row>
                            <Col span={12}>
                                <b>{item.balance_amt - item.cycle_in}</b>
                            </Col>
                        </Row>
                        <Row style={{
                            marginTop: '20px',
                        }}>
                            <Col span={12}>
                                <b>{item.reward_amt ? item.reward_amt : '0'}</b>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={12}>
                                <b>{item.cycle_out ? item.cycle_out : '0'}</b>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={12}>
                                <b>{item.total ? item.total : '0'}</b>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            )
        });
        return (
            <Row>
                <Col span={8}>
                    {listItemsLeft}
                </Col>
                <Col span={16}>
                    {listItemsRight}
                </Col>
                <Divider/>
            </Row>
        );
    }
}

function mapState(state) {
    console.log('state ', state);
    const {authentication, investments, transactions} = state;
    const {user} = authentication;

    return {user, transactions, investments};
}

function formatDate(date) {
    return new Date(date).toDateString() + ' ' + new Date(date).toLocaleTimeString();
}

const actionCreators = {
    getTransactions: userActions.getTransactions,
    getInvestments: userActions.getInvestments,
}

const connectedHomePage = connect(mapState, actionCreators)(HomePage);
export {connectedHomePage as HomePage};